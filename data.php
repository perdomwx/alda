<!DOCTYPE html>
<html lang="en">
<head>
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <title>Alda | Data</title>
  <?php require_once "inc/meta_css.php"; ?>
</head>
<body id="body-cont">
  <div class="container">
    <div class="navbar navbar-default" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button class="navbar-toggle collapsed" data-target="#div_navbar" data-toggle="collapse" type="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">Alda</a>
        </div> <!-- end .navbar-header -->
        <div class="navbar-collapse collapse" id="div_navbar">
          <ul class="nav navbar-nav">
            <li><a href="index.php">Home</a></li>
            <li class="active"><a href="data.php">Data</a></li>
          </ul> <!-- end .nav -->
        </div> <!-- end .navbar-collapse -->
      </div> <!-- end .container-fluid -->
    </div> <!-- end .navbar -->
    <div class="row">
      <div class="col-md-3">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Parameter(s)</h3>
          </div> <!-- end .panel-heading -->
          <div class="panel-body">
            <div id="div-reg-view">
              <div class="form-group">
                <label for="sel-reg">Region(s):</label>
                <select class="form-control" id="sel-reg-def">
                </select> <!-- end .form-control -->
              </div> <!-- end .form-group -->
              <div class="form-group">
                <select class="form-control" id="sel-reg-alt">
                </select> <!-- end .form-control -->
              </div> <!-- end .form-group -->
              <button class="btn btn-default" id="btn-tgl-reg" type="button">
                <i class="fa fa-plus" id="i-reg-tgl"></i>
              </button>
            </div> <!-- end #div-reg-view -->
            <div id="div-econ-view">
              <hr>
              <label>Economy and Growth:</label>
              <div class="checkbox">
                <label><input id="chk-cpi" type="checkbox" checked>CPI</label>
              </div> <!-- end .checkbox-->
              <div class="checkbox">
                <label><input id="chk-cpi-pc" type="checkbox" checked>CPI percent change</label>
              </div> <!-- end .checkbox-->
              <div class="checkbox">
                <label><input id="chk-gdp" type="checkbox" checked>GDP</label>
              </div> <!-- end .checkbox-->
              <div class="checkbox">
                <label><input id="chk-gdp-pc" type="checkbox" checked>GDP per capita</label>
              </div> <!-- end .checkbox-->
              <div class="checkbox">
                <label><input id="chk-pop" type="checkbox" checked>Population</label>
              </div> <!-- end .checkbox-->
              <div class="checkbox">
                <label><input id="chk-ppp" type="checkbox" checked>PPP</label>
              </div> <!-- end .checkbox-->
            </div>  <!-- end #div-grph-view -->
          </div> <!-- end .panel-body -->
        </div> <!-- end .panel -->
      </div> <!-- end .col-md-3 -->
      <div class="col-md-9" id="div-econ-dta">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Economy & Growth</h3>
          </div> <!-- end .panel-heading -->
          <div class="panel-body">
            <div id="div-cpi-wrap">
              <div class="alert alert-info alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <b>Consumer Price Index</b>: "Consumer price index reflects
                changes in the cost to the average consumer of acquiring a
                basket of goods and services that may be fixed or changed at
                specified intervals, such as yearly. The Laspeyres formula is
                generally used. <u>The base year that was used to calculate the
                CPI was 2005.</u>" - <b>World Bank</b>
              </div> <!-- end .alert -->
              <div id="div-cpi-chrt"></div>
            </div> <!-- end #div-cpi-wrap -->
            <div id="div-cpi-pc-wrap">
              <hr>
              <div class="alert alert-info alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <b>Consumer Price Index percent change</b>: "Inflation as
                measured by the consumer price index reflects the annual
                percentage change in the cost to the average consumer of
                acquiring a basket of goods and services that may be fixed or
                changed at specified intervals, such as yearly. The Laspeyres
                formula is generally used." - <b>World Bank</b>
              </div> <!-- end .alert -->
              <div id="div-cpi-pc-chrt"></div>
            </div> <!-- end #div-cpi-pc-wrap -->
            <div id="div-gdp-wrap">
              <hr>
              <div class="alert alert-info alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <b>Gross Domestic Product</b>: "GDP at purchaser's prices is the
                sum of gross value added by all resident producers in the
                economy plus any product taxes and minus any subsidies not
                included in the value of the products. It is calculated without
                making deductions for depreciation of fabricated assets or for
                depletion and degradation of natural resources. <u>Data are in
                current U.S. dollars.</u> Dollar figures for GDP are converted
                from domestic currencies using single year official exchange
                rates. For a few countries where the official exchange rate
                does not reflect the rate effectively applied to actual foreign
                exchange transactions, an alternative conversion factor is used.
                " - <b>World Bank</b>
              </div> <!-- end .alert -->
              <div id="div-gdp-chrt"></div>
            </div> <!-- end #div-gdp-wrap -->
            <div id="div-gdp-pc-wrap">
              <hr>
              <div class="alert alert-info alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <b>Gross Domestic Product per capita</b>: "GDP per capita is
                gross domestic product divided by midyear population. GDP is
                the sum of gross value added by all resident producers in the
                economy plus any product taxes and minus any subsidies not
                included in the value of the products. It is calculated without
                making deductions for depreciation of fabricated assets or for
                depletion and degradation of natural resources. <u>Data are in
                current U.S. dollars.</u>" - <b>World Bank</b>
              </div> <!-- end .alert -->
              <div id="div-gdp-pc-chrt"></div>
            </div>  <!-- end #div-gdp-pc-wrap -->
            <div id="div-pop-wrap">
              <hr>
              <div class="alert alert-info alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <b>Population</b>: "Total population is based on the de facto
                definition of population, which counts all residents regardless
                of legal status or citizenship--except for refugees not
                permanently settled in the country of asylum, who are generally
                considered part of the population of their country of origin.
                The values shown are midyear estimates." - <b>World Bank</b>
              </div> <!-- end alert -->
              <div id="div-pop-chrt"></div>
            </div> <!-- end #div-pop-wrap -->
            <div id="div-ppp-wrap">
              <hr>
              <div class="alert alert-info alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <b>Purchasing Power Parity</b>: "GDP per capita based on
                purchasing power parity (PPP). PPP GDP is gross domestic product
                 converted to international dollars using purchasing power
                 parity rates. An international dollar has the same purchasing
                 power over GDP as the U.S. dollar has in the United States.
                 GDP at purchaser's prices is the sum of gross value added by
                 all resident producers in the economy plus any product taxes
                 and minus any subsidies not included in the value of the
                 products. It is calculated without making deductions for
                 depreciation of fabricated assets or for depletion and
                 degradation of natural resources. <u>Data are in current
                 international dollars based on the 2011 ICP round.</u>
                 " - <b>World Bank</b>
              </div> <!-- end alert -->
              <div id="div-ppp-chrt"></div>
            </div> <!-- end #div-ppp-wrap -->
          </div> <!-- end .panel-body -->
        </div> <!-- end .panel -->
      </div> <!-- end .col-md-9 -->
    </div> <!-- end .row -->
    <div class="navbar navbar-default">
      <div class="navbar-header">
        <p class="navbar-text">&copy; 2015 Alda</p>
      </div> <!-- end .navbar-header -->
    </div> <!-- end .navbar -->
  </div> <!-- end .container -->
  <?php require_once "inc/meta_js.php"; ?>
  <!--  Google Charts Javascript -->
  <script type="text/javascript" src="https://www.google.com/jsapi"></script>
  <!-- Custom Javascript -->
  <script src="js/data.js"></script>
</body>
</html>
