<!DOCTYPE html>
<html lang="en">
<head>
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <title>Alda | Home</title>
  <?php require_once "inc/meta_css.php"; ?>
</head>
<body id="body-cont">
  <div class="container">
    <div class="navbar navbar-default" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button class="navbar-toggle collapsed" data-target="#div_navbar" data-toggle="collapse" type="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">Alda</a>
        </div> <!-- end .navbar-header -->
        <div class="navbar-collapse collapse" id="div_navbar">
          <ul class="nav navbar-nav">
            <li class="active"><a href="index.php">Home</a></li>
            <li><a href="data.php">Data</a></li>
          </ul> <!-- end .nav -->
        </div> <!-- end .navbar-collapse -->
      </div> <!-- end .container-fluid -->
    </div> <!-- end .navbar -->
    <div class="jumbotron">
      <h1>Alda</h1>
      <p>Alda is a web tool that aggregates several data points provided by the
        World Bank and displays the information graphically. The data is either
        an economic or a demographic marker of a region.</p>
    </div> <!-- end .jumbotron -->
    <div class="row">
      <div class="col-md-6">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h1 class="panel-title">Data</h1>
          </div> <!-- end .panel-heading -->
          <div class="panel-body">
            <p>Initially, the raw data came from the World Bank. However, the
              data was compiled into a usable format by the Open Knowledge
              foundation. If you find the data interesting or useful visit their
              website.</p>
            <a class="btn btn-default" href="http://data.okfn.org/data"><i class="fa fa-flask"></i> Open Knowledge</a>
          </div> <!-- end .panel-body -->
        </div> <!-- end .panel -->
      </div> <!-- end .col-md-6 -->
      <div class="col-md-6">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h1 class="panel-title">Source</h1>
          </div> <!-- end .panel-heading -->
          <div class="panel-body">
            <p>The source code for the service is open source and freely
              available on GitHub for your use. If you have any questions or
              concerns about the application please contact perdomwx through
              GitHub.</p>
            <a class="btn btn-default" href="https://www.github.com/perdomwx/alda"><i class="fa fa-github"></i> View Source</a>
          </div> <!-- end .panel-body -->
        </div> <!-- end .panel -->
      </div> <!-- end .col-md-6 -->
    </div> <!-- end .row -->
    <div class="navbar navbar-default">
      <div class="navbar-header">
        <p class="navbar-text">&copy; 2015 Alda</p>
      </div> <!-- end .navbar-header -->
    </div> <!-- end .navbar -->
  </div> <!-- end .container -->
  <?php require_once "inc/meta_js.php"; ?>
</body>
</html>
