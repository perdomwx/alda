/**
 * data.js - Alda graphing handler.
 */
 // Disable AJAX asynchronous mode.
$.ajaxSetup({
  async: false
});

// Load the Google Chart API.
google.load("visualization", "1", { packages: ["corechart"] });

// DOM JQuery Events
$(document).ready(function() {
  // Display all of the regions in the default selector.
  popReg("#sel-reg-def");
  drawUI();

  // Button Click Event
  $("#btn-tgl-reg").click(function() {
    // Hide the sel-reg-alt selector and empty its values when the component
    // has a class named "fa-minus".
    if ($("#i-reg-tgl").hasClass("fa-minus")) {
      $("#i-reg-tgl").removeClass("fa-minus").addClass("fa-plus");
      $("#sel-reg-alt").empty();
      drawUI();
    } else {
      // The default action is to remove "fa-plus" and add the "fa-minus" class
      // Region information is populated inside the sel_reg_alt selector.
      $("#i-reg-tgl").removeClass("fa-plus").addClass("fa-minus");
      popReg("#sel-reg-alt");
      drawUI();
    }

    // Toggle the additional region selector.
    $("#sel-reg-alt").toggle();
  });

  // These checkbox events hide their corresponding div and refresh the UI.
  $("#chk-cpi").change(function() {
    $("#div-cpi-wrap").slideToggle("slow");
    drawUI();
  });

  $("#chk-cpi-pc").change(function() {
    $("#div-cpi-pc-wrap").slideToggle("slow");
    drawUI();
  });

  $("#chk-gdp").change(function() {
    $("#div-gdp-wrap").slideToggle("slow");
    drawUI();
  });

  $("#chk-gdp-pc").change(function() {
    $("#div-gdp-pc-wrap").slideToggle("slow");
    drawUI();
  });

  $("#chk-pop").change(function() {
    $("#div-pop-wrap").slideToggle("slow");
    drawUI();
  });

  $("#chk-ppp").change(function() {
    $("#div-ppp-wrap").slideToggle("slow");
    drawUI();
  });

  // Refresh the UI when a user changes a region in either selector.
  $("#sel-reg-alt").change(function() {
    drawUI();
  });

  $("#sel-reg-def").change(function() {
    drawUI();
  });

  $(window).resize(function(){
    drawUI();
  });
});

// Javascript Functions

// drawUI() - Data from the selectors is used to call the Google Chart API.
function drawUI() {
  // All of the possible graphs.
  var chrt_lst = ["cpi", "cpi-pc", "gdp", "gdp-pc", "pop", "ppp"];

  // These variables contain values from the selectors.
  var reg = $("#sel-reg-def").val();
  var reg_sec = $("#sel-reg-alt").val();
  var reg_nme = $("#sel-reg-def option:selected").text();
  var reg_nme_sec = $("#sel-reg-alt option:selected").text();

  // Iterate drawing UI from array.
  for (i = 0; i < chrt_lst.length; i++) {
    drawData(chrt_lst[i], reg, reg_sec, reg_nme, reg_nme_sec);
  }
}

// drawData(ty, cde, cde_sec, nme, nme_sec) - This method draws the graph
// interface using the Google Chart API.
function drawData(ty, cde, cde_sec, nme, nme_sec) {
  var api_url;    // The location of the generated data.
  var chrt_div;   // The name of the div where that graph will be displayed.
  var chrt_opt;   // Google Chart Options https://goo.gl/cMvCmL
  var data;       // The generated data.
  var data_fmt;   // Format of the data. https://goo.gl/XDs8aM

  // Generate api_url, and the title based on the existance of the method
  // parameters.
  var cde_url = cde ? "reg=" + cde : "";
  var cde_sec_url = cde_sec ? "reg_sec=" + cde_sec : "";
  var nme_title = nme ? nme : "";
  var nme_sec_title = nme_sec ? " and " + nme_sec : "";

  // If data from the default selector is missing and the second selector has a
  // value, swap them.
  if (!cde && cde_sec) {
    cde = cde_sec;
    cde_sec = "";
  }

  // Determine the graph to generated based on the its type.
  // All of the possible graphs are "cpi", "cpi-pc", "gdp", "gdp-pc", "pop",
  // and "ppp".
  switch (ty) {
    case "cpi":
      api_url = "api/json-cpi.php?" + cde_url + "&" + cde_sec_url;
      chrt_div = "#div-cpi-chrt";

      chrt_opt = {
        title: "Consumer Price Index for " + nme_title + nme_sec_title,

        hAxis: {
          title: "Year",
          format: ""
        },

        vAxis: {
          title: "CPI",
          format: "short"
        }
      };

      data_fmt = {
        formatType: "decimal",
        fractionDigits: 1
      };

      break;
    case "cpi-pc":
      api_url = "api/json-cpi-pc.php?" + cde_url + "&" + cde_sec_url;

      chrt_div = "#div-cpi-pc-chrt";

      chrt_opt = {
        title: "Consumer Price Index percent change for " + nme_title + nme_sec_title,

        hAxis: {
          title: "Year",
          format: ""
        },

        vAxis: {
          title: "CPI percent change",
          format: "#%"
        }
      };

      data_fmt = {
        formatType: "decimal",
        pattern: "0.00%"
      };

      break;
    case "gdp":
      api_url = "api/json-gdp.php?" + cde_url + "&" + cde_sec_url;

      chrt_div = "#div-gdp-chrt";

      chrt_opt = {
        title: "Gross Domestic Product for " + nme_title + nme_sec_title,

        hAxis: {
          title: "Year",
          format: ""
        },

        vAxis: {
          title: "GDP",
          format: "short"
        }
      };

      data_fmt = {
        formatType: "decimal",
        prefix: "$"
      };

      break;
    case "gdp-pc":
      api_url = "api/json-gdp-pc.php?" + cde_url + "&" + cde_sec_url;

      chrt_div = "#div-gdp-pc-chrt";

      chrt_opt = {
        title: "Gross Domestic Product per capita for " + nme_title + nme_sec_title,

        hAxis: {
          title: "Year",
          format: ""
        },

        vAxis: {
          title: "GDP per capita",
          format: "short"
        }
      };

      data_fmt = {
        formatType: "decimal",
        prefix: "$"
      };

      break;
    case "pop":
      api_url = "api/json-pop.php?" + cde_url + "&" + cde_sec_url;

      chrt_div = "#div-pop-chrt";

      chrt_opt = {
        title: "Population for " + nme_title + nme_sec_title,

        hAxis: {
          title: "Year",
          format: ""
        },

        vAxis: {
          title: "Population",
          format: "short"
        }
      };

      data_fmt = {
        formatType: "decimal",
        fractionDigits: 0
      };

      break;
    case "ppp":
      api_url = "api/json-ppp.php?" + cde_url + "&" + cde_sec_url;

      chrt_div = "#div-ppp-chrt";

      chrt_opt = {
        title: "Purchasing Power Parity for " + nme_title + nme_sec_title,

        hAxis: {
          title: "Year",
          format: ""
        },

        vAxis: {
          title: "PPP",
          format: "short"
        }
      };

      data_fmt = {
        formatType: "decimal",
        fractionDigits: 2
      };

      break;
  }

  // Retrieve data for the chart from the api backend.
  var json = $.ajax({
    url: api_url,
    dataType:"json",
    async: false
  }).responseText;

  // Convert the JSON data into a DataTable.
  data = new google.visualization.DataTable(json);
  fmt = new google.visualization.NumberFormat(data_fmt);

  // Format the second column of data if default selector has data.
  if (cde) {
    fmt.format(data, 1);
  }

  // Format the third column of data if alternate selector has data.
  if (cde && cde_sec) {
    fmt.format(data, 2);
  }

  var chrt = new google.visualization.LineChart($(chrt_div).get(0));

  chrt.draw(data, chrt_opt);
}

// popReg(fld) - Populates region selectors with data.
function popReg(fld) {
  $.getJSON("api/json-reg.php", function(data) {
    $.each(data, function(key, val) {
      $(fld).append('<option value="'+ key +'">'+ val +'</option>');
    });
  });
}
