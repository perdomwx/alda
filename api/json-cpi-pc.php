<?php
/**
 * json-cpi-pc.php - Annual CPI percentage change for a region.
 */
// Retrieve the region code.
$reg = $_GET["reg"];
$reg_sec = $_GET["reg_sec"];

require_once "db.php";

// Lookup the "reg" region in the database and retrieve the cpi-pc data.
// The lookup will perform if and only if data is provided.
if (isset($reg)) {
  $que = $db->prepare("SELECT * FROM econ_cpi WHERE region_code = :reg ORDER BY year");
  $que->execute(array(":reg" => $reg));
  $res = $que->fetchAll();

  // Divide the new index over the old index minus one to calculate the rate of
  // change over two consecutive years.
  for($i = 0; $i < count($res) - 1; $i++) {
    $pc_res[] = array("year" => $res[$i][1], "cpi_pc" => ($res[$i+1][2] / $res[$i][2]) - 1);
  }
}

// Lookup the "reg_sec" region in the database and retrieve the cpi-pc data.
// The lookup will perform if and only if data is provided.
if (isset($reg_sec)) {
  $que = $db->prepare("SELECT * FROM econ_cpi WHERE region_code = :reg_sec ORDER BY year");
  $que->execute(array(":reg_sec" => $reg_sec));
  $res_sec = $que->fetchAll();

  // Divide the new index over the old index minus one to calculate the rate of
  // change over two consecutive years.
  for($i = 0; $i < count($res_sec) - 1; $i++) {
    $pc_res_sec[] = array("year" => $res_sec[$i][1], "cpi_pc" => ($res_sec[$i+1][2] / $res_sec[$i][2]) - 1);
  }
}

if (isset($reg) || isset($reg_sec)) {
  // Formatting the data to work with Google Charts.
  $cols = array(
    array("type" => "number", "label" => "Year")
  );

  if (isset($reg)) {
    $cols[] = array("type" => "number", "label" => "$reg");
  }

  if (isset($reg_sec)) {
    $cols[] = array("type" => "number", "label" => "$reg_sec");
  }

  $rows = array();

  if (isset($reg_sec)) {
    foreach($pc_res as $row) {
      foreach($pc_res_sec as $row_sec) {
        if ($row["year"] == $row_sec["year"]) {
          $tmp = array();
          $tmp[] = array("v" => $row["year"]);
          $tmp[] = array("v" => $row["cpi_pc"]);
          $tmp[] = array("v" => $row_sec["cpi_pc"]);
          $rows[] = array("c" => $tmp);
        }
      }
    }
  } else {
    foreach($pc_res as $row) {
      $tmp = array();
      $tmp[] = array("v" => $row["year"]);
      $tmp[] = array("v" => $row["cpi_pc"]);
      $rows[] = array("c" => $tmp);
    }
  }
  
  $tbl = array();
  $tbl["cols"] = $cols;
  $tbl["rows"] = $rows;

  // Print out the Google Chart data.
  echo json_encode($tbl);
}
?>
