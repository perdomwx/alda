<?php
/**
 * json-gdp-pc.php - Returns the GDP per capita data for a region.
 */
// Retrieve the region code.
$reg = $_GET["reg"];
$reg_sec = $_GET["reg_sec"];

require_once "db.php";

// Lookup the "reg" region in the database and retrieve the gdp-pc data.
// The lookup will perform if and only if data is provided.
if (isset($reg)) {
  $gdp_que = $db->prepare("SELECT * FROM econ_gdp WHERE region_code = :reg ORDER BY year");
  $gdp_que->execute(array(":reg" => $reg));
  $gdp_res = $gdp_que->fetchAll();

  // Lookup the region in the database and retrieve the population data.
  $pop_que = $db->prepare("SELECT * FROM dem_pop WHERE region_code = :reg ORDER BY year");
  $pop_que->execute(array(":reg" => $reg));
  $pop_res = $pop_que->fetchAll();

  // To calculate gdp per capita we divide the GDP by the size of the population
  // of a given year.
  $gdp_pc_dta = array();

  foreach($gdp_res as $gdp_row) {
    foreach($pop_res as $pop_row) {
      if ($gdp_row["year"] == $pop_row["year"]) {
        $gdp_pc_dta[] = array("year" => $gdp_row["year"], "pc" => $gdp_row["gdp"] / $pop_row["pop"]);
      }
    }
  }
}

// Lookup the "reg_sec" region in the database and retrieve the gdp-pc data.
// The lookup will perform if and only if data is provided.
if (isset($reg_sec)) {
  $gdp_que = $db->prepare("SELECT * FROM econ_gdp WHERE region_code = :reg_sec ORDER BY year");
  $gdp_que->execute(array(":reg_sec" => $reg_sec));
  $gdp_res_sec = $gdp_que->fetchAll();

  // Lookup the region in the database and retrieve the population data.
  $pop_que = $db->prepare("SELECT * FROM dem_pop WHERE region_code = :reg_sec ORDER BY year");
  $pop_que->execute(array(":reg_sec" => $reg_sec));
  $pop_res_sec = $pop_que->fetchAll();

  // To calculate gdp per capita we divide the GDP by the size of the population
  // of a given year.
  $gdp_pc_dta_sec = array();

  foreach($gdp_res_sec as $gdp_row_sec) {
    foreach($pop_res_sec as $pop_row_sec) {
      if ($gdp_row_sec["year"] == $pop_row_sec["year"]) {
        $gdp_pc_dta_sec[] = array("year" => $gdp_row_sec["year"], "pc" => $gdp_row_sec["gdp"] / $pop_row_sec["pop"]);
      }
    }
  }
}

if (isset($reg) || isset($reg_sec)) {
  // Formatting the data to work with Google Charts.
  $cols = array(
    array("type" => "number", "label" => "Year")
  );

  if (isset($reg)) {
    $cols[] = array("type" => "number", "label" => "$reg");
  }

  if (isset($reg_sec)) {
    $cols[] = array("type" => "number", "label" => "$reg_sec");
  }

  $rows = array();

  foreach ($gdp_pc_dta as $gdp_pc_row) {
    if (isset($reg_sec)) {
      foreach($gdp_pc_dta_sec as $gdp_pc_row_sec) {
        if ($gdp_pc_row["year"] == $gdp_pc_row_sec["year"]) {
          $tmp = array();
          $tmp[] = array("v" => $gdp_pc_row["year"]);
          $tmp[] = array("v" => $gdp_pc_row["pc"]);
          $tmp[] = array("v" => $gdp_pc_row_sec["pc"]);
          $rows[] = array("c" => $tmp);
        }
      }
    } else {
      $tmp = array();
      $tmp[] = array("v" => $gdp_pc_row["year"]);
      $tmp[] = array("v" => $gdp_pc_row["pc"]);
      $rows[] = array("c" => $tmp);
    }
  }

  $tbl = array();
  $tbl["cols"] = $cols;
  $tbl["rows"] = $rows;

  // Print out the Google Chart data.
  echo json_encode($tbl);
}
?>
