<?php
/**
* db_con.php - Initialize a connection to the database.
*/
$host = "localhost";
$user = "root";
$pass = "";
$dbnm = "alda";

try {
  $db = new PDO("mysql:host=$host;dbname=$dbnm", $user, $pass);
  // set the PDO error mode to exception
  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  // echo "Connected successfully";
} catch(PDOException $e) {
  echo "Connection failed: " . $e->getMessage();
}
?>
