<?php
/**
 * json-reg.php - Returns a list of regions with its code.
 */
require_once "db.php";

// Retrieve a list of regions from the World Bank.
$que = $db->prepare("SELECT * FROM region ORDER BY region_name");
$que->execute();
$res = $que->fetchAll(PDO::FETCH_COLUMN|PDO::FETCH_GROUP);

echo json_encode($res);
?>
