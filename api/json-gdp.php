<?php
/**
 * json-gdp.php - Returns the GDP data for a region.
 */
// Retrieve the region code.
$reg = $_GET["reg"];
$reg_sec = $_GET["reg_sec"];

require_once "db.php";

// Lookup the "reg" region in the database and retrieve the gdp data.
// The lookup will perform if and only if data is provided.
if (isset($reg)) {
  $que = $db->prepare("SELECT * FROM econ_gdp WHERE region_code = :reg ORDER BY year");
  $que->execute(array(":reg" => $reg));
  $res = $que->fetchAll();
}

// Lookup the "reg_sec" region in the database and retrieve the gdp data.
// The lookup will perform if and only if data is provided.
if (isset($reg_sec)) {
  $que = $db->prepare("SELECT * FROM econ_gdp WHERE region_code = :reg_sec ORDER BY year");
  $que->execute(array(":reg_sec" => $reg_sec));
  $res_sec = $que->fetchAll();
}

if (isset($reg) || isset($reg_sec)) {
  // Formatting the data to work with Google Charts.
  $cols = array(
    array("type" => "number", "label" => "Year")
  );

  if (isset($reg)) {
    $cols[] = array("type" => "number", "label" => "$reg");
  }

  if (isset($reg_sec)) {
    $cols[] = array("type" => "number", "label" => "$reg_sec");
  }

  $rows = array();

  foreach($res as $row) {
    if (isset($reg_sec)) {
      foreach($res_sec as $row_sec) {
        if ($row["year"] == $row_sec["year"]) {
          $tmp = array();
          $tmp[] = array("v" => $row["year"]);
          $tmp[] = array("v" => $row["gdp"]);
          $tmp[] = array("v" => $row_sec["gdp"]);
          $rows[] = array("c" => $tmp);
        }
      }
    } else {
      $tmp = array();
      $tmp[] = array("v" => $row["year"]);
      $tmp[] = array("v" => $row["gdp"]);
      $rows[] = array("c" => $tmp);
    }
  }

  $tbl = array();
  $tbl["cols"] = $cols;
  $tbl["rows"] = $rows;

  // Print out the Google Chart data.
  echo json_encode($tbl);
}
?>
